# Subsetting
#

# Clear the environment
rm(list=ls())

# ----------------------------------------------------------------------- Basic
# Operations to extract a subset of an object
# Single '[' extract an object of the same class
# Double '[[' extract elements of a list or data frame
# $ extract elements of a list or data frame by their name

# --------------------------------------------------------------------- Vectors

vector_of_char <- c("a", "b", "c", "d", "e", "a")

# Select one element
vector_of_char[2]
# Select more elements with indexing
vector_of_char[1:3]
# Select more elements with logic
vector_of_char[vector_of_char > "b"]

# ----------------------------------------------------------------------- Lists

list_of_char <- list(first = 1:4, second = "b", third = c(3.14, 6.28))

#
list_of_char[1]
class(list_of_char[1])

list_of_char[1:3]
class(list_of_char[1:3])

list_of_char["first"]
class(list_of_char["first"])

list_of_char$third
class(list_of_char$third)

list_of_char[[2]]
class(list_of_char[[2]])

list_of_char[[3]][[1]]
list_of_char[[c(3,1)]]

# ----------------------------------------------------------------------- Matrices

m <- matrix(1:6, 2, 3)
m

# Specify coordinates of one element. Gives a vector of length 1
m[2, 2]

# Specify coordinates of one element. Gives a matrix of size 1,
m[2, 2, drop = F]

# Specify set of coordinates
m[1:2, c(1, 3)]

# Do not specify all coordinates
m[1, ]
m[, 3]
m[, 2, drop = F]

# ------------------------------------------------------------ Partial Matching
# Useful to address objects with 'strange' name

x <- list(aasfrovbrnrrbalerjknalbverjiaknviejrnkvaelrjflisejrnlve = 5)

# With '$' notation R looks for partial name
x$a

# By default, double '[' requires exact name
x["a"]
x[["a", exact=F]]

# -------------------------------------------------------------- Missing values

# A vector with missing elements
x <- c(1, 2, 6, 3, NA, 2, NA, NA)
x

# Remove missing elements
x[!is.na(x)]

# Index only elemens available in multiple vectors
y <- c(4, NA, 9, NA, 3, 3, NA, NA)
x[complete.cases(x,y)]
y[complete.cases(x,y)]
