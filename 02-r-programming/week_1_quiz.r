# Week 1 Quiz

# Clear the environment
rm(list=ls())

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward  way to specify a
# path relative to this script file
if( ! file.exists("data/hw1_data.csv") )
{
   # Assume the working directory is RStudio project root
   working_directory <- getwd()
   setwd(paste(working_directory, "/", "02-r-programming", sep = ""))
}


# ----------------------------------------------------------------- Question 11

# Load the data
data = read.csv("hw1_data.csv")

# Display the column names
names(data)

# ----------------------------------------------------------------- Question 12

# First 2 rows of the table
data[1:2,]

# ----------------------------------------------------------------- Question 13

# Retrieve the number of rows
nrow(data)

# ----------------------------------------------------------------- Question 14

# Last two rows
tail(data, 2)

# ----------------------------------------------------------------- Question 15

# Row 47, column 'Ozone'
data[47, "Ozone"]

# ----------------------------------------------------------------- Question 16

# Sub-vector of NAs
length(data[["Ozone"]][is.na(data["Ozone"])])

# ----------------------------------------------------------------- Question 17

# Sub-vector without NAs
mean(data[["Ozone"]][!is.na(data["Ozone"])])

# ----------------------------------------------------------------- Question 18

# Rows where Ozone is not NA and > 31
oz_rows = (data["Ozone"] > 31) & (!is.na(data["Ozone"]))
# Rows where Temp is not NA and > 90
tp_rows = (data["Temp"] > 90) & (!is.na(data["Temp"]))
# Extract combined-valid rows and compute mean
mean(data[["Solar.R"]][oz_rows & tp_rows])

# ----------------------------------------------------------------- Question 19

# Sub-vector without NAs and Month = 6
mean(data[["Temp"]][(!is.na(data["Temp"])) & (data["Month"] == 6)])

# ----------------------------------------------------------------- Question 20

# Sub-vector without NAs and Month = 5
max(data[["Ozone"]][(!is.na(data["Ozone"])) & (data["Month"] == 5)])



