# Profiler
#

# Clear the environment
rm(list=ls())

# ----------------------------------------------------------------- Dependences

# ----------------------------------------------------------------- System Time
# Evaluates the time in seconds to evaluate the given expression

# The elapsed time is higher than the user time since the expression waits for
# external network response (no cpu involved)
system.time(readLines("https://www.google.com"))

# The user time may be higher than the elapsed time since the expression could
# be execute on multiple processors (much cpu involved)
system.time(svd(matrix(rnorm(1000*1000), 1000, 1000)))

# ------------------------------------------------------------------ R Profiler
# Takes regular snapshots of the stack (e.g. every 0.02s) and tabulates time in
# each function
Rprof(NULL)
data <- matrix(rnorm(1000*1000), 1000, 1000)
Rprof(append = TRUE)
x <- svd(data)
Rprof(NULL)
summaryRprof()
