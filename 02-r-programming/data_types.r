# R Console Input and Evaluation
#

# -------------------------------------------------------------- Entering Input

# Assignment (nothing gets displayed)
x <- 1

# Dynamic type
msg <- "hello"

# -------------------------------------------------------------------- Printing

# Explicit and implicit print (automatic print)
print(x)
x

# The square brackets shown as output message represent the element position.
# Show this creating an array (only if the Console window is not enoug wide)
x <- 1:50
x

# --------------------------------------------------------------------- Numbers

# Double-precision number
class(1)
# Integer
class(1L)
# Complex
class(1i)
# Character
class("a")
# Logical
class(TRUE)
class(T)
# Infinity
class(Inf)
# Undefined value
class(NaN)

# --------------------------------------------------------------- Missing value
# Missing values can be of any type. Not-A-Number (NaN) is Not-Available (NA)
# but NA is not NaN

x <- c(1, 2, NA, 10, 3, NaN)
is.na(x)
is.nan(x)

# --------------------------------------------------------------------- Vectors
# A vector is the most basic object, which can contain objects of only one class

# Empty vector
v <- vector()

# Allocate vector
v <- vector("numeric", length = 10)

# Function 'c' to create vector of objects
v <- c(0.1, 0.6)
v <- c(TRUE, FALSE)
v <- c("a", "bcdefghi")
v <- c(1+0i, 0+4i)

# -------------------------------------------------------------------- Coercion
# Coercion occurst if objects are mixed

# Vector of mixed object -> all objects are implicitely converted
v <- c(1, "a", 1i, T)
class(v)
class(v[1])
v[4]

# Explicit coercion with 'as'
v <- 1:20
as.numeric(v)
as.character(v)
as.logical(v)

# Coercion does not always work
v <- c("a", "b", "c")
as.numeric(v)

# ----------------------------------------------------------------------- Lists
# A list is similar to a vector but can contain objects of different classes

# List indexing uses double brackets
x <- list(1, 1i, "1", T)
x
x[3][1]
x[[3]]

# -------------------------------------------------------------------- Matrices
# Matrices are vectors with a dimension attribute

# Allocate memory for matrix
m <- matrix(nrow = 2, ncol = 4)

# Assign memory for matrix. Values are row-based assigned
m <- matrix(1:8, nrow = 2, ncol = 4)

# Matrix dimensions
dim(m)

# Convert vector to matrix setting its dimensions
m <- 1:6
m
dim(m) <- c(2, 3)
m

# Matrix created combining vectors by columns or by row
x <- 1:3
y <- 10:12
cbind(x, y)
rbind(x, y)

# --------------------------------------------------------------------- Objects
# Everything in R is an object

# R objects can have attributes
attributes(m)

# All R object can have names
x <- 1:3
names(x)
names(x) <- c("first", "second", "third")
x
x["second"]

# Lists can have names for each element
l <- list(first = 1, second = 2, third = 3)
l
l["third"]

# Matrices can have names
m <- matrix(1:4, nrow = 2, ncol = 2)
dimnames(m) <- list(c("row1", "row2"), c("col1", "col2"))
m

# --------------------------------------------------------------------- Factors
# Factors are used to represent categorical data. It is like attaching a label
# to a variable

# Supply a vector to create an unordered factor
f <- factor(c("yes", "yes", "no", "yes", "no"))
f
# The unique values are levels. The table function count their occurrence
table(f)

# Strip out the class converting the Factor to a vector with 'levels' attribute
x <- unclass(f)
x

# Order the level explicitely (otherwise uses 'no' as first level since it
# comes before alphabetically)
f <- factor(c("yes", "yes", "no", "yes", "no"),
            levels = c("yes", "no"))

# ----------------------------------------------------------------- Data frames
# Used to store tabular information. Should be seen as a list of elements, all
# of the same length.

# Data frame with two columns and named columns
d <- data.frame( foo = 1:4, bar = c(T,T,F,F))
d

# Access one columnt
d["foo"]

# Number of rows and columns
nrow(d)
ncol(d)
