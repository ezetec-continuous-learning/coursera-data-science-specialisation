# Simulation
#

# Clear the environment
rm(list=ls())

# ----------------------------------------------------------------- Dependences

# --------------------------------------------------- Generating Random Numbers
# Four set of functions do exist for each probability distribution:
# - r<...> Random variable generator
# - d<...> Evaluate the density
# - p<...> Evaluate cumulative
# - q<...> Evaluate quantile (inverse of p<...>)

# Initialise the seed
set.seed(1)

# <...>norm - Normal random variables
rnorm(10, 5, 2)
dnorm(10, 0, 1)
pnorm(0, 0, 1)
qnorm(0, 0, 1)

# pois - Poisson random variables
rpois(10, 5)
dpois(4, 5)
ppois(4, 5)
qpois(0.3, 5)

# --------------------------------------------------- Simulating a Linear Model

# Generation of time series from model:
#    y = b0 + b1*x + e
# where is e = N(0, 2^2), x = N(0, 1^2), b0 = 0.5 and b1 = 2
set.seed(20)
b0 <- 0.5
b1 <- 2
x <- rnorm(100, 0, 1)
e <- rnorm(100, 0, 2)
expr <- expression(b0 + b1 * x + e)
y <- eval(expr)
summary(y)
plot(x, y)

# What if x is binary?
x <- rbinom(100, 1, 0.5)
y <- eval(expr)
summary(y)
plot(x, y)

# Assume a variable with Poisson distribution
#    Y = Poisson(mu)
# where log(mu) = b0 + b1 * x and b0 = 0.5 and b1 = 0.3
set.seed(20)
b0 <- 0.5
b1 <- 0.3
x <- rnorm(100, 0, 1)
log.mu <- b0 + b1 * x
y <- rpois(100, exp(log.mu))
summary(y)
plot(x, y)

# ------------------------------------------------------------- Random Sampling
# Draw randomly from a set

# Sample without repetition
sample(1:10, 4)
sample(letters, 4)

# Sample with repetition
sample(1:10, 10, replace = T)
sample(letters, 10, replace = T)

# Obtain permutation without specifying the number of samples
sample(1:10)
