# Loop functions
#

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward  way to specify a
# path relative to this script file
if( ! file.exists("data/hw1_data.csv") )
{
   # Assume the working directory is RStudio project root
   working_directory <- getwd()
   setwd(paste(working_directory, "/", "02-r-programming", sep = ""))
}
# ---------------------------------------------------------------------- lapply
# Loop over a list and apply a function to each element

# Compute mean of list elements
x <- list(a = 1:5, b = rnorm(10), c = rnorm(10, 1), d = rnorm(100, 5))
lapply(x, mean)

# Pass arguments to the function
x <- 1:4
lapply(x, runif, min = 0, max = 10)

# Apply a custom function to extract first column of arrays
x <- list(a = matrix(1:4, 2, 2), b = matrix(1:6, 3, 2))
lapply(x, function(mtx) mtx[,1])

# ---------------------------------------------------------------------- sapply
# Same as lapply but 'simplify' the result (e.g. return a vector instead of a
# list if all elements are of length 1)

# Compute mean of list elements getting a vector
x <- list(a = 1:5, b = rnorm(10), c = rnorm(10, 1), d = rnorm(100, 5))
sapply(x, mean)

# ---------------------------------------------------------------------- apply
# Apply function over the margins of array. It is often used over matrices

# Compute the mean of each column of a matrix 20x10
x <- matrix(rnorm(200), 20, 10)
apply(x, 2, mean)

# Compute the mean of each row
apply(x, 1, mean)

# Compute for each row the 25th and 75th quantile
apply(x, 1, quantile, probs = c(0.25, 0.75))

# Average of 3D matrix in an array
x <- array(rnorm(2*2*10), c(2,2,10))
apply(x, c(1,2), mean)

# ---------------------------------------------------------------------- mapply
# Multivariate version of lapply (apply a function over a set of inputs)

# Create in a more compact way the following list:
# list(rep(1, 4), rep(2, 3), rep(3, 2), rep(4, 1))
#       [[1, 1, 1, 1], [2, 2, 2], [3, 3], [4]]
mapply(rep, 1:4, 4:1)

# Vectorising a function that could not accept arrays
noise <- function(n, mean, sd) {
   rnorm(n, mean, sd)
}
noise(5, 1, 2)
# noise(1:5, 1:5, 2) Still gives 5 elements
mapply(noise, 1:5, 1:5, 2)

# ---------------------------------------------------------------------- tapply
# Apply function over subset of vector

# Compute the range and mean over subsets of the array
x <- c(rnorm(10), runif(10), rnorm(10,1))
f <- gl(3, 10)
tapply(x, f, mean)
tapply(x, f, range)

# -------------------------------------------------------------- split & lapply
# Function 'split' takes a factor vector, as tapply does, to separate the data

# Split the air quality data by month
airquality = read.csv("data/hw1_data.csv")
head(airquality)
s <- split(airquality, airquality$Month)

# Compute the mean quality among each month
lapply(s, function(x) colMeans(x[c("Ozone", "Solar.R", "Wind")], na.rm = T))
sapply(s, function(x) colMeans(x[c("Ozone", "Solar.R", "Wind")], na.rm = T))

# Splitting in more than one level
x <- rnorm(10)
f1 <- gl(2, 5)
f2 <- gl(5, 2)
f <- interaction(f1, f2)
str(split(x, f, drop = T))
