# Control structures
#

# Clear the environment
rm(list=ls())

# --------------------------------------------------------------------- IF-ELSE

# If without else
condition = T
if ( condition ) {
   print("Condition 1 true")
}


# The 'else' must be on the same line as the closing bracket!
if ( condition ) {
   print("Condition 2 true")
} else {
   print("Condition 2 false")
}

# Multiple else
value <- 100
if ( value < 0 ) {
   print("Negative value")
} else if ( value > 10 ) {
   print("Very positive value")
} else {
   print("Positive value")
}

# Alternative form highlighting assignment
x <- -3
x <- if ( x < 0 ) {
   -x
}
x

# ------------------------------------------------------------------------- FOR

# Loop over elements of vector
for (i in 1:10) {
   print(i)
}
for (s in c("a", "b", "c", "d", "e")) {
   print(s)
}

# Loop over index addressing vector
x <- c("a", "b", "c", "d", "e")
for (i in seq_along(x)) {
   print(paste(i, x[i]))
}

# ----------------------------------------------------------------------- WHILE

# Simple while structure
count <- 0
while ( count < 10 ) {
   count <- count + 1
   print(count)
}

# ------------------------------------------------------------------------ NEXT

# Next skips one iteration
count <- 0
while ( count < 10 ) {
   count <- count + 1
   if ( count == 5 ) {
      next
   }
   print(count)
}

# ----------------------------------------------------------------------- BREAK

# Breaks quits the loop
count <- 0
while ( count < 10 ) {
   count <- count + 1
   if ( count == 5 ) {
      break
   }
   print(count)
}

# ---------------------------------------------------------------------- REPEAT

# Repeat is the infinite loop (loop without condition, e.g. while(T))
count <- 0
repeat {
   if ( count > 10 )
   {
      break
   } else {
      count <- count + 1
   }
   print(count)
}


