# Programming Assignment 1 INSTRUCTIONS: Air Pollution
#

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward  way to specify a
# path relative to this script file
if( ! dir.exists("data/specdata") )
{
   # Assume the working directoy is RStudio project root
   working_directory <- getwd()
   setwd(paste(working_directory, "/", "02-r-programming", sep = ""))
}

# Free variable to enable logs
log = F

# ---------------------------------------------------- 'pollutantmean' function
# Calculates the mean of a pollutant (sulfate or nitrate) across a specified
# list of monitors.

pollutantmean <- function(directory, pollutant, id = 1:332) {
   # directory - indicates the location of the CSV files
   # pollutant - name of the pollutant, either "sulfate" or "nitrate"
   # id        - monitor ID numbers to be used

   # Initialise measurement vector
   meas <- vector()

   # Iterate over each ID to extract the measurements
   for (idx in id) {
      file_path = paste(directory, "/", sprintf("%03d",idx), ".csv", sep="")
      if (log) print(file_path)
      data <- read.csv(file_path)

      meas <- c(meas, data[[pollutant]])
   }

   # Returned value
   if (length(meas) > 0) mean(meas, na.rm=T)
   else NA
}

# --------------------------------------------------------- 'complete' function
# Reports the number of completely observed cases in each data file. Return a
# data frame where the first column is the name of the file and the second
# column is the number of complete cases.

complete <- function(directory, id = 1:332) {
   # directory - indicates the location of the CSV files
   # id        - monitor ID numbers to be used

   # Initialise variables
   summary <- data.frame()

   # Iterate over each ID to extract the measurements
   for (idx in id) {
      file_path = paste(directory, "/", sprintf("%03d",idx), ".csv", sep="")
      if (log) print(file_path)
      data <- read.csv(file_path)

      valid_num <- sum(apply(data, 1, function(row) {all(!is.na(row))}))
      if (valid_num > 0) {
         summary <- rbind(summary, data.frame(id = idx, nobs = valid_num))
      }
   }

   # Returned value
   summary
}

# ------------------------------------------------------------- 'corr' function
# Calculates the correlation between sulfate and nitrate for monitor locations.

corr <- function(directory, threshold = 0) {
   # directory - indicates the location of the CSV files
   # threshold - minimum number of complete cases in the data file to be used

   # Initialise variables
   correlations <- vector()

   # Retrieve the IDs to complete cases table and filter based on threshold
   info <- complete(directory)
   id <- info[info[["nobs"]] > threshold, ][["id"]]
   if(log) print(id)

   # Iterate over each ID to extract the measurements
   for (idx in id) {
      file_path = paste(directory, "/", sprintf("%03d",idx), ".csv", sep="")
      if (log) print(file_path)
      data <- read.csv(file_path)

      correlations <- c(correlations, cor(x   = data[["sulfate"]],
                                          y   = data[["nitrate"]],
                                          use = "complete.obs"))
   }

   # Returned value
   correlations
}

# ----------------------------------------------------------- run demo commands

test1 <- pollutantmean("data/specdata", "sulfate", 1:10)
test1
all.equal(test1, 4.064128)

test2 <- pollutantmean("data/specdata", "nitrate", 70:72)
test2
all.equal(test2, 1.706047)

test3 <- pollutantmean("data/specdata", "nitrate", 23)
test3
all.equal(test3, 1.280833)

test4 <- complete("data/specdata", 1)
test4
all.equal(test4, data.frame(id = 1, nobs = 117))

test5 <- complete("data/specdata", c(2, 4, 8, 10, 12))
test5
all.equal(test5, data.frame(id = c(2, 4, 8, 10, 12),
                            nobs = c(1041, 474, 192, 148, 96)))

test6 <- complete("data/specdata", 30:25)
test6
all.equal(test6, data.frame(id = c(30, 29, 28, 27, 26, 25),
                            nobs = c(932, 711, 475, 338, 586, 463)))

test7 <- complete("data/specdata", 3)
test7
all.equal(test7, data.frame(id = 3, nobs = 243))

test8 <- corr("data/specdata", 150)
test8
all.equal(test8, c(-0.01895754, -0.14051254, -0.04389737,
                   -0.06815956, -0.12350667, -0.07588814))
summary(test8)

test9 <- corr("data/specdata", 400)
test9
all.equal(test9, c(-0.01895754,-0.04389737, -0.06815956,
                   -0.07588814,  0.76312884, -0.15782860))
summary(test9)

test10 <- corr("data/specdata", 5000)
test10
length(test10) == 0
summary(test10)

test11 <- corr("data/specdata")
test11
length(test11) == 323
summary(test11)

# ------------------------------------------------------------------------ quiz

pollutantmean("data/specdata", "sulfate", 1:10)

pollutantmean("data/specdata", "nitrate", 70:72)

pollutantmean("data/specdata", "sulfate", 34)

pollutantmean("data/specdata", "nitrate")

cc <- complete("data/specdata", c(6, 10, 20, 34, 100, 200, 310))
print(cc$nobs)

cc <- complete("data/specdata", 54)
print(cc$nobs)

RNGversion("3.5.1")
set.seed(42)
cc <- complete("data/specdata", 332:1)
use <- sample(332, 10)
print(cc[use, "nobs"])

cr <- corr("data/specdata")
cr <- sort(cr)
RNGversion("3.5.1")
set.seed(868)
out <- round(cr[sample(length(cr), 5)], 4)
print(out)

cr <- corr("data/specdata", 129)
cr <- sort(cr)
n <- length(cr)
RNGversion("3.5.1")
set.seed(197)
out <- c(n, round(cr[sample(n, 5)], 4))
print(out)

cr <- corr("data/specdata", 2000)
n <- length(cr)
cr <- corr("data/specdata", 1000)
cr <- sort(cr)
print(c(n, round(cr, 4)))
