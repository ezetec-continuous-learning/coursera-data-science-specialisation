# Use user-defined functions
#

# Clear the environment
rm(list=ls())

if( ! file.exists("data/functions.r") )
{
   # Assume the working directory is RStudio project root
   working_directory <- getwd()
   setwd(paste(working_directory, "/", "02-r-programming", sep = ""))
}

# ----------------------------------------------------------------- DEPENDENCES

# Import the custom functions
source("data/functions.r")

# Load external data
data = read.csv("data/hw1_data.csv")

# --------------------------------------------------------------------- EXECUTE

# Sum two numbers
add2(1, 1)

# Retrive only elements above 10
above10(c(1, 3, 5, 7, 9, 11, 13, 15, 17, 19))
above(c(1, 3, 5, 7, 9, 11, 13, 15, 17, 19))

# Retrive only elements above 15
above(x = c(1, 3, 5, 7, 9, 11, 13, 15, 17, 19),
      n = 15)

# Compute mean of columns
columnmean(data)

# Discover which arguments have the function and if they have defaults
formals(columnmean)

# Positional matching: non-formal (unnamed) arguments are matched positionally
columnmean(data, F)
columnmean(y = data, removeNA = F)
columnmean(removeNA = F, data)
columnmean(removeNA = F, y = data)

# Arguments partial (unsafe) match
columnmean(data, r = F)
columnmean(data, r = T)

# ... is used to indicate a variable number of arguments
args(paste)
# Arguments ... do not use partial matching
paste("a", "b", sep = ":")
paste("a", "b", se = ":")

# Lexical scoping is used to create two custom function
square <- make.power(2)
cube <- make.power(3)
square(2)
cube(2)


