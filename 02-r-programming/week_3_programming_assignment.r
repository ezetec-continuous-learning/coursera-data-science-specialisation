# Programming Assignment 2: Lexical Scoping
#

# Clear the environment
rm(list=ls())

# ----------------------------------------------------------------- Dependences

# --------------------------------------- Example: Caching the Mean of a Vector

# Creates a special "vector", which is really a list containing a function to
# - set the value of the vector
# - get the value of the vector
# - set the value of the mean
# - get the value of the mean
makeVector <- function(x = numeric()) {
   m <- NULL
   set <- function(y) {
      x <<- y
      m <<- NULL
   }
   get <- function() x
   setmean <- function(mean) m <<- mean
   getmean <- function() m
   list(set = set, get = get,
        setmean = setmean,
        getmean = getmean)
}

# calculates the mean of the special "vector" created with the above function
cachemean <- function(x, ...) {
   m <- x$getmean()
   if(!is.null(m)) {
      message("getting cached data")
      return(m)
   }
   data <- x$get()
   m <- mean(data, ...)
   x$setmean(m)
   m
}

# Use
testVect <- function() {
   vect <- makeVector()
   vect$set(1:5000)
   cachemean(vect)
   cachemean(vect)
}

# --------------------------------- Assignment: Caching the Inverse of a Matrix
# Write the following functions:

# - makeCacheMatrix: This function creates a special "matrix" object that can
#   cache its inverse.
makeCacheMatrix <- function(mtx = matrix()) {
   mtxInv <- NULL

   set <- function(m) {
      mtx <<- m
      mtxInv <<- NULL
   }
   get <- function() mtx
   setinv <- function(inv) mtxInv <<- inv
   getinv <- function() mtxInv

   list(set = set, get = get, setinv = setinv, getinv = getinv)
}

# - cacheSolve: This function computes the inverse of the special "matrix"
#   returned by makeCacheMatrix above. If the inverse has already been
#   calculated (and the matrix has not changed), then the cachesolve should
#   retrieve the inverse from the cache.
cacheSolve <- function(mtx, ...) {
   mtxInv <- mtx$getinv()
   if(!is.null(mtxInv)) {
      message("getting cached data")
   } else {
      mtxInv <- solve(mtx$get())
      mtx$setinv(mtxInv)
   }
   mtxInv
}

# Use
testMtx <- function(size = 5) {
   mtx <- makeCacheMatrix()
   mtx$set(matrix(runif(size*size), size, size))
   print(mtx$get())
   print(cacheSolve(mtx))
   print(cacheSolve(mtx))
   print(round(mtx$getinv() %*% mtx$get(), digits=9))
}
