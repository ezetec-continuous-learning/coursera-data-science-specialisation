# Programming Assignment 3: Quiz
#

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward  way to specify a
# path relative to this script file
if( ! dir.exists("data") )
{
   # Assume the working directoy is RStudio project root
   working_directory <- getwd()
   setwd(paste(working_directory, "/", "02-r-programming", sep = ""))
}

# ----------------------------------------------------------------- Dependences

# Read data from table (import as characters to avoid trailing zeros of IDs to
# be removed)
hospital <- read.csv("data/hospital-data.csv",
                     header = T,
                     colClasses = "character")
outcome <- read.csv("data/outcome-of-care-measures.csv",
                    colClasses = "character")
str(hospital)
str(outcome)

# ------------------------------------------------------------------- Chapter 1

# Convert column "Hospital.30.Day.Death..Mortality..Rates.from.Heart.Attack"
# from string to numbers
outcome[, 11] <- as.numeric(outcome[, 11])

# Display the corresponding hystogram
hist(outcome[, 11])

# ------------------------------------------------------------------- Chapter 2
# Finding the best hospital in a state

# Returns a character vector with the name of the hospital that has the best
# (i.e. lowest) 30-day mortality for the specified outcome in that state
#
# Args:
#    state   the 2-character abbreviated name of a state
#    outcome an outcome name
#
best <- function(state, outcome) {

   ## Read outcome data

   data <- read.csv("data/outcome-of-care-measures.csv",
                    colClasses = "character")
   data <- split(data, data$State)

   ## Check that state and outcome are valid

   # List of valid inputs
   out2col <- c("heart attack"  = 11,
                "heart failure" = 17,
                "pneumonia"     = 23)
   # Check input
   if ( ! is.element(state, names(data)) )  {
      stop("invalid state")
   }
   if ( ! is.element(outcome, names(out2col))) {
      stop("invalid outcome")
   }

   # Select the data from the given state
   state_data <- data[[state]]
   # Convert the requested outcome to numeric
   state_data[, out2col[outcome]] <- as.numeric(state_data[, out2col[outcome]])
   # Sort by the requested outcome and hospital name
   state_data <- state_data[order(state_data[, out2col[outcome]],
                                  state_data[, "Hospital.Name"],
                                  decreasing = F,
                                  na.last = T),]

   ## Return hospital name in that state with lowest 30-day death

   hospital <- state_data[1, "Hospital.Name"]

   ## rate

   hospital
}

# ------------------------------------------------------------------- Chapter 3
# Ranking hospitals by outcome in a state

# Returns a character vector with the name of the hospital that has the ranking
# specified by the num argument
#
# Args:
#   state   the 2-character abbreviated name of a state
#   outcome an outcome name
#   num     the ranking of a hospital in that state for that outcome
#
rankhospital <- function(state, outcome, num = "best") {

   ## Read outcome data

   data <- read.csv("data/outcome-of-care-measures.csv",
                    colClasses = "character")
   data <- split(data, data$State)

   ## Check that state and outcome are valid

   # List of valid inputs
   out2col <- c("heart attack"  = 11,
                "heart failure" = 17,
                "pneumonia"     = 23)
   # Check input
   if ( ! is.element(state, names(data)) )  {
      stop("invalid state")
   }
   if ( ! is.element(outcome, names(out2col)) ) {
      stop("invalid outcome")
   }
   if ( ! is.element(num, c("best", "worst"))
        & ! is.numeric(num) ) {
      stop("invalid num")
   }

   ## Return hospital name in that state with lowest 30-day death

   # Select the data from the given state
   state_data <- data[[state]]
   # Convert the requested outcome to numeric
   state_data[, out2col[outcome]] <- as.numeric(state_data[, out2col[outcome]])
   # Sort by the requested outcome and hospital name
   state_data <- state_data[order(state_data[, out2col[outcome]],
                                  state_data[, "Hospital.Name"],
                                  decreasing = F,
                                  na.last = T),]
   # Remove rows with NA
   state_data <- state_data[!is.na(state_data[out2col[outcome]]), ]

   if( is.character(num) & num == "best") num <- 1
   if( is.character(num) & num == "worst") num <- nrow(state_data)
   if( num <= nrow(state_data) ) {
      hospital <- state_data[num, "Hospital.Name"]
   } else {
      hospital <- NA
   }

   ## rate

   hospital
}

# ------------------------------------------------------------------- Chapter 4
# Ranking hospitals in all states

# Returns a 2-column data frame containing the hospital in each state that has
# the ranking specified in num
#
# Args:
#    outcome an outcome name
#    num     the ranking of a hospital in that state for that outcome
#
rankall <- function(outcome, num = "best") {

   ## Read outcome data

   data <- read.csv("data/outcome-of-care-measures.csv",
                    colClasses = "character")

   ## Check that state and outcome are valid

   # List of valid inputs
   out2col <- c("heart attack"  = 11,
                "heart failure" = 17,
                "pneumonia"     = 23)
   # Check input
   if ( ! is.element(outcome, names(out2col)) ) {
      stop("invalid outcome")
   }
   if ( ! is.element(num, c("best", "worst"))
        & ! is.numeric(num) ) {
      stop("invalid num")
   }

   ## For each state, find the hospital of the given rank

   # Extract hospital function
   hospital <- function(sts) {
      # Convert to numeric
      sts[, out2col[outcome]] <- as.numeric(sts[, out2col[outcome]])
      # Sort the data
      sts <- sts[order(sts[, out2col[outcome]],
                       sts[, "Hospital.Name"],
                       decreasing = F,
                       na.last = T),]
      # Remove NAs
      sts <- sts[!is.na(sts[out2col[outcome]]), ]
      # Get hospital name
      if( is.character(num) ) {
         if( num == "best" ) {
            num <- 1
         } else {
            num <- nrow(sts)
         }
      }
      if( num <= nrow(sts) ) {
         h <- sts[num, "Hospital.Name"]
      } else {
         h <- NA
      }
      h
   }

   # List of hospitals
   hospitals <- sapply(split(data, data$State), hospital)

   ## Return a data frame with the hospital names and the
   frame <- data.frame(
      hospital  = hospitals,
      state     = names(hospitals),
      row.names = names(hospitals)
   )

   ## (abbreviated) state name
   frame
}

# ---------------------------------------------- Programming Assignment 3: Quiz

# 1
best("SC", "heart attack")
# 2
best("NY", "pneumonia")
# 3
best("AK", "pneumonia")
# 4
rankhospital("NC", "heart attack", "worst")
# 5
rankhospital("WA", "heart attack", 7)
# 6
rankhospital("TX", "pneumonia", 10)
# 7
rankhospital("NY", "heart attack", 7)
# 8
r <- rankall("heart attack", 4)
as.character(subset(r, state == "HI")$hospital)
# 9
r <- rankall("pneumonia", "worst")
as.character(subset(r, state == "NJ")$hospital)
# 10
r <- rankall("heart failure", 10)
as.character(subset(r, state == "NV")$hospital)

