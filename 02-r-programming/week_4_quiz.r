# Week 4 Quiz

# Clear the environment
rm(list=ls())

# ----------------------------------------------------------------- Dependences


# ------------------------------------------------------------------ Question 1
# What is produced at the end of this snippet of R code?

set.seed(1)
rpois(5, 2)

# ------------------------------------------------------------------ Question 2
# What R function can be used to generate standard Normal random variables?

rnorm(10, 0, 1)

# ------------------------------------------------------------------ Question 3
# When simulating data, why is using the set.seed() function important? Select
# all that apply.

# Allows generating the same set of pseudo-random variables

# ------------------------------------------------------------------ Question 4
# Which function can be used to evaluate the inverse cumulative distribution
# function for the Poisson distribution?

ppois(5, 1)
qpois(ppois(5, 1), 1)

# ------------------------------------------------------------------ Question 5
# What does the following code do?

set.seed(10)
x <- rep(0:1, each = 5)
e <- rnorm(10, 0, 20)
y <- 0.5 + 2 * x + e

# ------------------------------------------------------------------ Question 6
# What R function can be used to generate Binomial random variables?

rbinom(10, 5, 0.3)

# ------------------------------------------------------------------ Question 7
# What aspect of the R runtime does the profiler keep track of when an R
# expression is evaluated?

# The Stack

# ------------------------------------------------------------------ Question 8
# Consider the following R code

library(datasets)
Rprof()
fit <- lm(y ~ x1 + x2)
Rprof(NULL)

# ------------------------------------------------------------------ Question 9
# When using 'system.time()', what is the user time?

# Time spent by the CPU

# ------------------------------------------------------------------ Question 10
# If a computer has more than one available processor and R is able to take
# advantage of that, then which of the following is true when using
# 'system.time()'?

# The user time may be larger than the elapsed time
