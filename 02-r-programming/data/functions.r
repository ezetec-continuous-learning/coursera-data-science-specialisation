
# =============================================================================

# Sum two numbers
add2 <- function(x, y) {
   # Last expression is returned
   x + y
}

# =============================================================================

# Returns values of vector above 10
above10 <- function(x) {
   use <- x > 10
   # Last expression is returned
   x[use]
}

# =============================================================================

# Let specify above which value are returned elements (with default value)
above <- function(x, n = 10) {
   use <- x > n

   # Last expression is returned
   x[use]
}

# =============================================================================

# Compute mean value along columns
# y : DataFrame or Matrix
columnmean <- function(y, removeNA = T) {
   # Calculate the number of columns
   nc <- ncol(y)
   # Initialise the returned vector
   means <- numeric(nc)
   # Loop over the columns
   for ( i in 1:nc ) {
      means[i] <- mean(y[, i], na.rm = removeNA)
   }
   # Last expression is returned
   means
}

# =============================================================================

# Personalise the plot function by specifying a default argument
my_plot <- function(x, y, type = "1", ...) {
   plot(x, y, type = type, ...)
}

# ============================================================= LEXICAL SCOPING

# Function generation
make.power <- function(n) {
   pow <- function(x) {
      x ^ n
   }
   pow
}
