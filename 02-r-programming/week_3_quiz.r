# Week 3 Quiz

# Clear the environment
rm(list=ls())

# ----------------------------------------------------------------- Dependences

library(datasets)
data("iris")
data("mtcars")

# ------------------------------------------------------------------ Question 1
# There will be an object called 'iris' in your workspace. In this dataset,
# what is the mean of 'Sepal.Length' for the species virginica?

Q <- round(mean(iris[iris$Species == "virginica",1]))
sprintf("Answer 1: %d", Q)

# ------------------------------------------------------------------ Question 2
# Continuing with the 'iris' dataset from the previous Question, what R code
# returns a vector of the means of the variables 'Sepal.Length', 'Sepal.Width',
# 'Petal.Length', and 'Petal.Width'?

Q <- expression(apply(iris[,1:4], 2, mean))
sprintf("Answer 2: %s", Q)
eval(Q)

# ------------------------------------------------------------------ Question 3
# How can one calculate the average miles per gallon (mpg) by number of
# cylinders in the car (cyl)? Select all that apply.

Q <- expression(tapply(mtcars$mpg, mtcars$cyl, mean))
sprintf("Answer 3: %s", Q)
eval(Q)
Q <- expression(sapply(split(mtcars$mpg, mtcars$cyl), mean))
sprintf("Answer 3: %s", Q)
eval(Q)
Q <- expression(with(mtcars, tapply(mpg, cyl, mean)))
sprintf("Answer 3: %s", Q)
eval(Q)

# ------------------------------------------------------------------ Question 4
# Continuing with the 'mtcars' dataset from the previous Question, what is the
# absolute difference between the average horsepower of 4-cylinder cars and
# the average horsepower of 8-cylinder cars?

Q <- round(diff(array(with(mtcars, tapply(hp, cyl, mean))[c("4", "8")])))
sprintf("Answer 4: %s", Q)

# ------------------------------------------------------------------ Question 5
# If you run 'debug(ls)', what happens when you next call the 'ls' function?

debug(ls)
ls()
undebug(ls)
