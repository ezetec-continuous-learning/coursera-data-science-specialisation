# Reading data
#

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward  way to specify a
# path relative to this script file

if( ! file.exists("data/reading_data.r") )
{
   # Assume the working directoy is RStudio project root
   working_directory <- getwd()
   setwd(paste(working_directory, "/", "02-r-programming", sep = ""))
}

# ---------------------------------------------------------------- Tabular Data

# Read data from table
tbl <- read.table("data/reading_data.txt", header = T)
tbl

# Read data from CSV (same as read.table but the default separator is ',')
csv <- read.csv("data/reading_data.csv", header = F, comment = "#")
csv

# Read lines from text file
txt <- readLines("data/reading_data.txt")
txt

# Read R source code
r <- source("data/reading_data_variables.r")
a_variable_in_the_other_file

# Load previously saved workspaces
load("data/reading_data_workspace.rData")
variable_in_workspace

# ---------------------------------------------------------------- Large Tables
# Large dataset could be a problem for 'read.table'

# Check how much RAM the data requires
n_of_rows          = 1500000
n_of_columns       = 120
size_of_element    = 16
overhead_percent   = 100
required_gigabytes = ((n_of_rows*n_of_columns*size_of_element/1024/1024/1024)
                     * (1 + overhead_percent/100))
required_gigabytes

# Supply the data classes to speed up data loading with key 'colClasses'
tbl <- read.table("data/reading_data.txt", header = T,
                  colClasses = c("numeric"))

# Otherwise read just the first lines, get the identified classes and apply them
tbl <- read.table("data/reading_data.txt", header = T,
                  nrows = 2)
classes <- sapply(tbl, class)
classes
tbl <- read.table("data/reading_data.txt", header = T,
                  colClasses = classes)

# ---------------------------------------------------------------Textual Format
# Data can be stored and retrived in textual format, like how databases work
# (e.g. SQL)

# R object
x = data.frame(a=1, b=2)
x

# Convert R object in textual form and display it
dput(x)

# Store the object textual conversion to a file
dput(x, "data/reading_data_textual_object.rData")
readLines("data/reading_data_textual_object.rData")

# Load an object stored in textual form
y <- dget("data/reading_data_textual_object.rData")

# Similar to dput, dump allows storing multiple objects by providing their
# name
dump(c("x", "y"), "data/reading_data_textual_object.rData")
readLines("data/reading_data_textual_object.rData")

