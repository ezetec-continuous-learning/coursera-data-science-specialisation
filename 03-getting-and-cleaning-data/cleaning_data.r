# Cleaning Data

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# ------------------------------------------------------------------ Subsetting

# Create data as if it was imported. Make example reproducible by fixing the
# generated 'random' data. Scrumble the variables and put some NAs
set.seed(13435)
X <- data.frame("var1" = sample(1:5),
                "var2" = sample(6:10),
                "var3" = sample(11:15))
X <- X[sample(1:5), ]
X$var2[c(1, 3)] = NA

# Subset a column number (get column as vector)
X[, 1]

# Subset a column name (get column as vector)
X[, "var1"]

# Logical subsettings
X[X$var2 > 8, ]

# Logical subsetting avoiding NAs
X[which(X$var2 > 8), ]

# --------------------------------------------------------------------- Sorting

# Extract sorted column
sort(X$var2)

# Extract sorted column keeping NAs at the end
sort(X$var2, na.last = T)

# Column-based sort by subsetting
X[order(X$var1), ]

# Multiple-column-based sort by subsetting
X[order(X$var1, X$var2), ]

# Alternatively the plyr package may help
require(plyr)
arrange(X, var1)
arrange(X, desc(var2), var1)

# ------------------------------------------------------------ Add rows/columns

# By assignment
X$var4 <- rnorm(5)

# By binding
cbind(X, "newCol" = rnorm(nrow(X)))
rbind(X, "newRow" = rnorm(ncol(X)))

# ----------------------------------------------------------------- Summarising

# The following dataset will be used for the demonstration
#filePath <- paste("data", "baltimore-data.csv", sep = "/")
filePath <- paste("data", "baltimore-data.json", sep = "/")
if ( !file.exists(filePath) )
{
#   fileUrl <- "https://data.baltimorecity.gov/api/views/k5ry-ef3g/rows.csv?accessType=DOWNLOAD"
   fileUrl <- "https://opendata.baltimorecity.gov/egis/rest/services/NonSpatialTables/Licenses/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
# Load the data using CSV as the separator is "," by default
#restData <- read.csv(filePath)
require("jsonlite")
restData <- fromJSON(txt = filePath)[["features"]][["attributes"]]

# Explore the dataset
head(restData, 3)
tail(restData, 3)
summary(restData)
str(restData)

# Draw some statistics
quantile(restData$LicenseFee,
         probs = c(.1, .2, .3, .4, .5, .6, .7, .8, .9, 1))

# Make tables from specific values
table(restData$AddrZip,
      useNA = "ifany")
table(restData$AddrZip,
      restData$LicenseFee,
      useNA = "ifany")

# Check for missing values
sum(is.na(restData$LicenseNumber))
any(is.na(restData$LicenseStatus))
colSums(is.na(restData))

# Values with specific characteristics
table(restData$AddrZip %in% c("21213"))
restData[restData$AddrZip %in% c("21213"),]


# The following datasets will be used for the demonstration
data("UCBAdmissions")
UCB <- as.data.frame(UCBAdmissions)
summary(UCB)
data("warpbreaks")
WRP <- as.data.frame(warpbreaks)
summary(WRP)

# Cross table (identify where the relationship exist)
xtabs(Freq ~ Gender + Admit, data = UCB)

# Flat tables to summarise bettere large tables
WRP$replicate <- rep(1:9,
                     len = 54)
xt <- xtabs(breaks ~., data = WRP)
xt
ftable(xt)

# ------------------------------------------------------ Creating new variables
# The 'restData dataset will be used for the demonstration

# Sequences are often used to index data. s1 specifies the first and last
# element and the increment between the values of the sequence.
s1 <- seq(1, 10,
          by = 2)

# s2 specifies the number of elements into the sequence instead of the increment
# value
s2 <- seq(1, 10,
          length = 3)

# s3 is a sequence created to index another vector
x <- c(1, 3, 8, 25, 100)
s3 <- seq(along = x)

# Create a new boolean column based on another column
restData$nearMe <- restData$AddrStreet %in% c("6412-16 FRANKFORD AVENUE")
str(restData)

# Create a new binary column based on another column
restData$zipWrong <- ifelse(restData$AddrZip < 0, T, F)
table(restData$zipWrong, restData$AddrZip < 0)

# Create a new group column based on another column
restData$zipGroups <- cut(as.numeric(restData$AddrZip),
                          breaks = quantile(as.numeric(restData$AddrZip)))
table(restData$zipGroups)
table(restData$zipGroups, restData$AddrZip)

# The Hmisc library provides an improved version of the cut function in which
# the given vector may be split based on quantiles by default and the
# left-endpoints are inclusive
require("Hmisc")
restData$zipGroups2 <- cut2(as.numeric(restData$AddrZip),
                            g = 4)
table(restData$zipGroups2)
table(restData$zipGroups2, restData$AddrZip)

# Convert numeric data in factor variables
restData$zcf <- factor(as.numeric(restData$AddrZip))

# New tables can be created as variation of the original ones with the 'mutate'
# function
require("plyr")
restData2 <- mutate(restData,
                    zipGroups = cut2(as.numeric(AddrZip), g=4))

# -------------------------------------------------------------- Reshaping data

require("reshape2")

# Elongate a data.frame by melting it. This way each measurement column gets
# stacked onto each other by creating the "variable" and "value" columns
mtcars$carname <- rownames(mtcars)
carMelt <- melt(mtcars,
                id           = c("carname", "gear", "cyl"),
                measure.vars = c("mpg", "hp"))
head(carMelt)

# The elongate data.frame can be re-casted. By default the 'dcast' function
# provides the count number (occurrence)
cylData <- dcast(carMelt, cyl ~ variable)
head(cylData)

# The elongate data.frame can be re-casted. The 'dcast' function can be passed
# as additional aggregation function as argument
cylData <- dcast(carMelt, cyl ~ variable, mean)
head(cylData)

# Apply functions - all-in-one
head(InsectSprays)
tapply(InsectSprays$count, InsectSprays$spray, sum)

# Apply functions - one-at-time
spIns <- split(InsectSprays$count, InsectSprays$spray)
sprCount <- lapply(spIns, sum)
unlist(sprCount)

# Apply functions - split and apply-combine-at-once
spIns <- split(InsectSprays$count, InsectSprays$spray)
sapply(spIns, sum)

# plyr: the split-apply-combine paradigm for R. See ?plyr
require("plyr")
ddply(InsectSprays,                  # .data
      .(spray),                      # .variables
      plyr::summarize,               # .fun
      sum = sum(count))              # argument passed to .fun
