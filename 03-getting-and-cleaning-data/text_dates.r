# Text and dates

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# -------------------------------------------------------------- Text variables

# The following dataset will be used for the demonstration
#filePath <- paste("data", "baltimore-data.csv", sep = "/")
filePath <- paste("data", "baltimore-data.json", sep = "/")
if ( !file.exists(filePath) )
{
   #   fileUrl <- "https://data.baltimorecity.gov/api/views/dz54-2aru/rows.csv?accessType=DOWNLOAD"
   fileUrl <- "https://opendata.baltimorecity.gov/egis/rest/services/NonSpatialTables/Licenses/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
# Load the data using CSV as the separator is "," by default
#restData <- read.csv(filePath)
library("jsonlite")
restData <- fromJSON(txt = filePath)[["features"]][["attributes"]]

# Show column names
names(restData)

# Make lowercase
tolower(names(restData))

# Split variable names
strsplit(names(restData), "\\_")[[21]]
strsplit(names(restData), "\\_")[[21]][1]
strsplit(names(restData), "\\_")[[21]][2]

# Substitute characters (use gsub to replace all occurrences)
sub("_", " ", names(restData)[[21]])

# Finding the rows containing string
grep("LOUNGE", restData$TradeName)
grep("LOUNGE", restData$TradeName,
     value = T)
table(grepl("LOUNGE", restData$TradeName))

# Count the length of the string
nchar("Weeeeee")

# Extract a substring by index
substr("This is", 1, 4)

# Remove heading and trailing spaces
library("stringr")
str_trim("    Hey    ")

# --------------------------------------------------------- Regular Expressions

# Match the start of a sentence: ^
"^i think"
# matches sentences starting with "i think"

# Match the end of a sentence: $
"morning$"
# matches sentences ending with "morning"

# Character classes: []
"[Bb][Uu][Ss][Hh]"
# matches all forms of "bush" (Bush, buSH, BuSh...)

# Ranges with character classes: [<1>-<2>]
"^[1-9]"
# matches sentences starting with a number

# Exluding character classes: [^]
"[^1-9]$"
# matches sentences not ending with a number

# Match any character: .
"9.11"
# Matches all sentences with a "9" followed by a character and "11". E.g. "9/11"

# Alternatives: |
"flood|fire"
# matches sentences containing the words "flood" or "fire"

# Alternatives can be expressions
"^[Gg]ood|[Bb]ad"
# Matches sentences starting with "Good" or "good" or sentences containing "Bad"
# or "bad"

# Sub-expressions: ()
"^([Gg]ood|[Bb]ad)"
# as in previous example, but "bad" or "Bad" must stay at the beginning

# Optional expressions: ?
"[Gg]eorge( [Ww]\.)? [Bb]ush"
# matches "George Bush" and "George W. Bush". The "." character is escaped.

# Any number: *
"(.*)"
# matches a sentences containing brackets, either empty or with text

# At least one: +
"[0-9]+"
# matches sentences with at least one number

# Interval quantifiers: {m,n}
"[0-9]{1,5}"
# matches a sentence containing a sequence of numbers of length 1 to 5

# Refer to sub-expression: \n
"([a-zA-Z]+) +\1"
# matches a sentence containing a word repeated twice in sequence

# ----------------------------------------------------------------------- Dates

# Gather current time and date as string
date()
class(date())

# Date object can be reformatted at will
Sys.Date()
class(Sys.Date())

# Gate data with defined format
format(Sys.Date(), "%a %b %d")

# Character vectors can be converted to date object by providing their format
twoDates <- c("1jan1960", "31jul1467")
as.Date(twoDates,
        "%d%b%Y")

# With date objects also computations are possible
diff(as.Date(twoDates, "%d%b%Y"))

# The Lubridate package helps date-related operations
library("lubridate")
mdy("08/04/2013")
dmy("23101854")
ymd_hms("2001-08-03 11:14:38")
