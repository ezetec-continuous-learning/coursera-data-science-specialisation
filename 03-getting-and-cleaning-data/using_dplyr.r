# Using dplyr

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# ----------------------------------------------------------------------- dplyr

# The 'dplyr' package improves the 'plyr' package without inventing anything
# new. It works with data.frame as default, assuming each row is an observation
# of the variables given in columns.

# It provides the following functions
# - select       Return a subset of the columns
# - filter       Extract a subset of the rows
# - arrange      Reorder the columns
# - mutate       Transforms or add new variables
# - rename       Rename variables
# - summarise    Prints statistics

library("dplyr")

# Data used for demonstration
filePath <- paste("data", "chicago.rds", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://github.com/DataScienceSpecialization/courses/raw/master/03_GettingData/dplyr/chicago.rds"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
chicago <- readRDS(filePath)

# Info about the data.frame
str(chicago)

# ---------------------------------------------------------------------- select


# Get a subset columns specifying the range
head(select(chicago, city:dptp))

# Get a subset columns excluding the range
head(select(chicago, -(city:dptp)))

# ---------------------------------------------------------------------- filter

# Select rows depending on one column value
head(filter(chicago, pm25tmean2 > 30))

# Select rows depending on multiple columns values
head(filter(chicago, pm25tmean2 > 30 & tmpd > 80))

# --------------------------------------------------------------------- arrange

# Reorder in ascending order
head(arrange(chicago, date), 3)
tail(arrange(chicago, date), 3)

# Reorder in descending order
head(arrange(chicago, desc(date)), 3)
tail(arrange(chicago, desc(date)), 3)

# ---------------------------------------------------------------------- rename

# Rename a column
head(rename(chicago,
            pm25     = pm25tmean2,
            dewpoint = dptp))

# ---------------------------------------------------------------------- mutate

# Create a new variable with the mean subtracted
chicago <- mutate(chicago,
                  pm25detrend = pm25tmean2 - mean(pm25tmean2, na.rm = T))
head(filter(select(chicago, pm25detrend), !is.na(pm25detrend)))

# Create a factor variable according to a threshold on another variable
chicago <- mutate(chicago,
                  tempcat = factor(tmpd > 80,
                                   labels = c("cold", "hot")))
head(chicago)

# ------------------------------------------------------------------- summarise

# Create a summary grouping variables
hotcold <- group_by(chicago, tempcat)
summarise(hotcold,
          pm25 = mean(pm25tmean2, na.rm = T),
          o3   = max(o3tmean2, na.rm = T),
          no2  = median(no2tmean2, na.rm = T))

# Categorise a dataset based on date
years <- group_by(mutate(chicago, year = as.POSIXlt(date)$year + 1900),
                  year)
summarise(years,
          pm25 = mean(pm25tmean2, na.rm = T),
          o3   = max(o3tmean2, na.rm = T),
          no2  = median(no2tmean2, na.rm = T))

# --------------------------------------------------------- chaining operations

# Chain operations with the %>% operator
chicago %>%
   mutate(month = as.POSIXlt(date)$mon + 1)  %>%
   group_by(month) %>%
   summarise(pm25 = mean(pm25tmean2, na.rm = T),
             o3   = max(o3tmean2, na.rm = T),
             no2  = median(no2tmean2, na.rm = T))

