# Week 4 Quiz

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# Dependences
library("dplyr")

# ------------------------------------------------------------------ Question 1
#  What is the value of the 123 element of the resulting list?


# Download the data if it does not already exists
filePath <- paste("data", "american-community-survey-4.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2Fss06hid.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
acs4 <- read.csv(filePath)

# Apply strsplit() to split all the names of the data frame on the characters
# "wgtp".
answ1 <- strsplit(names(acs4),
                  "wgtp")[[123]]

# ------------------------------------------------------------------ Question 2
# What is the average?

# Download the data if it does not already exists
filePath <- paste("data", "gdp-190-4.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2FGDP.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
gdp4 <- read.csv(filePath,
                 header = F,
                 na.strings = "")

# Convert data to tibble
gdp4 <- as_tibble(gdp4)

# Remove the commas from the GDP numbers in millions of dollars
gdp4 <- gdp4 %>%
   rename(USD = V5) %>%
   rename(CountryCode = V1) %>%
   rename(Ranking = V2) %>%
   filter(!is.na(CountryCode)) %>%
   filter(!is.na(Ranking)) %>%
   mutate(USD = trimws(USD)) %>%
   mutate(USD = as.numeric(gsub(",", "", USD)))

# Average them
answ2 <- mean(gdp4$USD, na.rm = T)

# ------------------------------------------------------------------ Question 3
# How many countries begin with United?

# In the data set from Question 2 what is a regular expression that would allow
# you to count the number of countries whose name begins with "United"?
reg_exp <- "^United"

# Assume that the variable with the country names in it is named countryNames
gdp4 <- gdp4 %>%
   rename(countryNames = V4)

# Get answer
answ3a <- reg_exp
answ3b <- length(grep(reg_exp, gdp4$countryNames))
answ3 <- list(answ3a, answ3b)

# ------------------------------------------------------------------ Question 4
# Of the countries for which the end of the fiscal year is available, how many
# end in June?

filePath <- paste("data", "edu-4.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2FEDSTATS_Country.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
edu4 <- read.csv(filePath,
                 na.strings = "")

# Convert data to tibble
edu4 <- as_tibble(edu4)

# Clean EDU by
# - remove rows without valid CountryCode
edu4 <- filter(edu4, !is.na(CountryCode))

# Ensure there are no duplicated countries
edu4 <- edu4 %>%
   distinct(CountryCode, .keep_all = T)

# Match the data based on the country shortcode.
# Merge the two data tables
mrg4 <- as_tibble(
   merge(gdp4, edu4,
         by   = "CountryCode",
         all  = F))

# Countries for which the end of the fiscal year is available
mrg4 <- mrg4 %>%
   filter(grepl("Fiscal year end:", Special.Notes)) %>%
   mutate(FYEMonth = gsub(".*: ([a-zA-Z]+) [0-9]+;.*", "\\1", Special.Notes)) %>%
   mutate(FYEMonth = factor(FYEMonth, levels = month.name))

# Get answer
answ4 <- nrow(filter(mrg4, FYEMonth == "June"))

# ------------------------------------------------------------------ Question 5
# How many values were collected in 2012? How many values were collected on
# Mondays in 2012?

# Use the following code to download data on Amazon's stock price and get the
# times the data was sampled.
library(quantmod)
amzn = getSymbols("AMZN",auto.assign=FALSE)
sampleTimes = index(amzn)

# Get answer
answ5a <- sum(format(sampleTimes, "%Y") == "2012")
answ5b <- sum((format(sampleTimes, "%Y") == "2012") &
                 format(sampleTimes, "%u") == 1)
answ5 <- list(answ5a, answ5b)


# --------------------------------------------------------------------- Summary

sprintf("Answer 1: '%s'", paste0(answ1, collapse = " "))
sprintf("Answer 2: '%s'", paste0(answ2, collapse = " "))
sprintf("Answer 3: '%s'", paste0(answ3, collapse = " "))
sprintf("Answer 4: '%s'", paste0(answ4, collapse = " "))
sprintf("Answer 5: '%s'", paste0(answ5, collapse = " "))

