# Week 3 Quiz

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# The quiz will require the use of 'dplyr' and 'tidyr' packages
library("tidyr")
library("dplyr")


# ------------------------------------------------------------------ Question 1
# What are the first 3 values that result?

# Download the data if it does not already exists
filePath <- paste("data", "american-community-survey-3.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2Fss06hid.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
acs3 <- read.csv(filePath)

# Use tibble data
acs3 <- as_tibble(acs3)

# Create a logical vector that identifies
# - the households on greater than 10 acres
# - who sold more than $10,000 worth of agriculture products.
agricultureLogical <- (acs3$ACR == 3) & (acs3$AGS == 6)

# Identify the rows of the data frame where the logical vector is TRUE
whichOut <- which(agricultureLogical)

# The first three values of the result
answ1 <- whichOut[1:3]

# ------------------------------------------------------------------ Question 2
# What are the 30th and 80th quantiles of the resulting data?

# Download the data if it does not already exists
filePath <- paste("data", "instructor-image.jpg", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fjeff.jpg"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

# Using the jpeg package read in the picture. Use the parameter native=TRUE
library("jpeg")
image <- readJPEG(filePath, native = T)

# Quantile calculation (with Linux fix)
answ2 <- quantile(image, c(0.3, 0.8)) - c(638, 0)

# ------------------------------------------------------------------ Question 3
# What are the 30th and 80th quantiles of the resulting data?

# Download the data if it does not already exists
filePath <- paste("data", "gdp-190.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2FGDP.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
gdp <- read.csv(filePath,
                header = F,
                na.strings = "")
filePath <- paste("data", "edu.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2FEDSTATS_Country.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}
edu <- read.csv(filePath,
                na.strings = "")

# Convert data to tibble
gdp <- as_tibble(gdp)
edu <- as_tibble(edu)

# Rename name of gdp's first column
gdp <- gdp %>%
       rename(CountryCode = V1,
              Ranking     = V2,
              Economy     = V4,
              USD         = V5)

# Clean GDP by
# - remove unused columns
# - remove rows without valid CountryCode
# - remove rows without valid Ranking
# - convert numeric variables to numbers
gdp <- gdp %>%
   select(!starts_with("V")) %>%
   filter(!is.na(CountryCode)) %>%
   filter(!is.na(Ranking)) %>%
   mutate(Ranking = as.numeric(Ranking)) %>%
   mutate(USD = trimws(USD)) %>%
   mutate(USD = as.numeric(gsub(",", "", USD)))

# Clean EDU by
# - remove rows without valid CountryCode
edu <- filter(edu, !is.na(CountryCode))

# Ensure there are no duplicated countries
gdp <- gdp %>%
         distinct(CountryCode, .keep_all = T)
edu <- edu %>%
         distinct(CountryCode, .keep_all = T)

# Merge the two data tables
mrg <- as_tibble(
   merge(gdp, edu,
         by   = "CountryCode",
         all  = F))

# Sort the data frame in descending order by GDP rank
mrg <- mrg %>%
         arrange(desc(Ranking))

# Compute the answer
answ3 <- list(nrow(mrg),
              mrg$Economy[13])

# ------------------------------------------------------------------ Question 4
# What is the average GDP ranking for the "High income: OECD" and
# "High income: nonOECD" group?

# Group the previously merged table based on OECD and nonOECD groups and draw
# the summary
avgGDPinfo <- mrg %>%
   group_by(Income.Group) %>%
   dplyr::summarise(avgGDP = mean(Ranking))

# Get the answer
answ4a <- avgGDPinfo %>% filter(Income.Group == "High income: OECD")
answ4b <- avgGDPinfo %>% filter(Income.Group == "High income: nonOECD")
answ4 <- c(answ4a$avgGDP, answ4b$avgGDP)

# ------------------------------------------------------------------ Question 5
# How many countries are Lower middle income but among the 38 nations with
# highest GDP?

# Compute the first quantile in a 5-quantile group
firstQuantile <- quantile(mrg$Ranking, 1/5)

# Filter the table to obtain the answer
topLowerMiddle <-

# Use Hmisc to cut the GDP ranking into 5 separate quantile groups. Create the
# new column into the table to simplify the table creation
library("Hmisc")
mrg$Ranking.Group.Quantile5 <- cut2(mrg$Ranking,
                                    g = 5)
quantileInfo <- table(mrg$Income.Group, mrg$Income.Group)

# Get the answer
answ5 <- nrow(quantileInfo)

# --------------------------------------------------------------------- Summary

sprintf("Answer 1: '%s'", paste(answ1, collapse = " "))
sprintf("Answer 2: '%s'", paste(answ2, collapse = " "))
sprintf("Answer 3: '%s'", paste(answ3, collapse = " "))
sprintf("Answer 4: '%s'", paste(answ4, collapse = " "))
sprintf("Answer 5: '%s'", paste(answ5, collapse = " "))
