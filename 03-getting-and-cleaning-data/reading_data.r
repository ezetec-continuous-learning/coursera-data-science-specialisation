# Reading data

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# ----------------------------------------------------------------------- MySQL

# Load the library needed to handle MySQL data
library("RMySQL")

# Retrieve information from an online database without actually downloading it
# (using Human Genome database from http://genome.ucsc.edu/ as an example)
ucscDbConnection <- dbConnect(drv  = MySQL(),
                              user = "genome",
                              host = "genome-euro-mysql.soe.ucsc.edu")
# Print the list of all databases available at UCSC server
ucscDbQueryResult <- dbGetQuery(conn      = ucscDbConnection,
                                statement = "show databases;")
ucscDbQueryResult
# Remember to always close the connection a the end
dbDisconnect(ucscDbConnection)

# Connect to a specific database and list all its tables
ucscDbHg19Connection <- dbConnect(drv  = MySQL(),
                                  user = "genome",
                                  host = "genome-euro-mysql.soe.ucsc.edu",
                                  db   = "hg19")
# Print information about the available tables in database HG19
ucscDbHg19AllTables <- dbListTables(conn = ucscDbHg19Connection)
class(ucscDbHg19AllTables)
ucscDbHg19AllTables[1:5]
# Show the field (columns) of a specific table
dbListFields(conn = ucscDbHg19Connection,
             name = "affyU133Plus2")
# Display the number of records (rows) in a specific table
dbGetQuery(conn      = ucscDbHg19Connection,
           statement = "select count(*) from affyU133Plus2")
# Retrieve an entire table
h19AffyU133Plus2Table <- dbReadTable(conn = ucscDbHg19Connection,
                                     name = "affyU133Plus2")
head(h19AffyU133Plus2Table)
# Retrieve a sub-set of the table
query <- dbSendQuery(
   conn      = ucscDbHg19Connection,
   statement = "select * from affyU133Plus2 where misMatches between 1 and 3")
h19AffyU133Plus2TableSubset <- fetch(query)
head(h19AffyU133Plus2TableSubset)
quantile(h19AffyU133Plus2TableSubset$misMatches)
dbClearResult(query)
# Close the connection
dbDisconnect(ucscDbHg19Connection)

# ----------------------------------------------------------------------- HDF5

library("hdf5r")

# Create an h5 file
h5FilePath <- paste("data", "test.h5", sep = "/")
if( file.exists(h5FilePath) )
{
   file.remove(h5FilePath)
}
h5FileConn <- h5file(h5FilePath)
h5flush(h5FileConn)

# Create data groups
h5FileConn$create_group("foo")
h5FileConn$create_group("bar")
h5FileConn$create_group("foo/bar")

# Write data into a group
A <- matrix(1:5*2, 5, 2)
h5FileConn[["foo/A"]] <- A
h5FileConn[["foo/A"]]
B <- array(seq(0.1, 2.0, by=0.1), dim=c(5,2,2))
h5FileConn[["foo/bar/B"]] <- B
h5FileConn[["foo/bar/B"]]$read()

# Modify data
h5FileConn[["foo/A"]]$write(list(1:nrow(A), 1:ncol(A)), A*A)

# Show the h5 object information
h5FileConn$ls(recursive=T)

# Flush data to disk and close the file
h5FileConn$flush()
h5FileConn$close()

# ----------------------------------------------------------------- Webscraping

# Retrieve the HTML content of a website
website <- "https://scholar.google.com/citations?user=hpnMQKwAAAAJ&hl=en"
urlConn <- url(website)
htmlCode <- readLines(urlConn)
close(urlConn)

library("XML")

# Even better, use XML to parse the HTML content
htmlTree <- htmlTreeParse(htmlCode,
                          useInternalNodes = T)

# Show the page title
xpathApply(htmlTree, "//title", xmlValue)

# Show the citation count of articles
xpathApply(htmlTree, "//td[@class='gsc_a_c']", xmlValue)

