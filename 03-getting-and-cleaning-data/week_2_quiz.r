# Week 2 Quiz

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# ------------------------------------------------------------------ Question 1
# What time was it created?

library("httr")
library("httpuv")

# Discover endpoints
oauth_endpoints("github")

# Declare application
myapp <- oauth_app(appname = "Week_2_Quiz",
                   key     = "51b82bf33da3c3cddf10",
                   secret  = "a753a91be3086ed26b7baf4923cac504b614933e")

# Get OAuth credentials
github_token <- oauth2.0_token(oauth_endpoints("github"), myapp)

# Authorize
#req <- GET("https://github.com/login/oauth/authorize", gtoken)
#stop_for_status(req)
#content(req)

# Retrieve data
gtoken <- config(token = github_token)
req <- GET("https://api.github.com/users/jtleek/repos", gtoken)

# Take action on http error
stop_for_status(req)

# Extract content from request and convert to data.frame
json1 = content(req)
gitDF = jsonlite::fromJSON(jsonlite::toJSON(json1))

# Get answer
answ1 <- gitDF[gitDF$full_name == "jtleek/datasharing", "created_at"]

# ------------------------------------------------------------------ Question 2
# Which of the following commands will select only the data for the probability
# weights pwgtp1 with ages less than 50?

library("sqldf")

# Download the data if it does not already exists
filePath <- paste("data", "american-community-survey.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2Fss06pid.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

# Load the data using CSV as the separator is "," by default
acs <- read.csv(filePath)

# Executed question commands
cmds2 <- c("select * from acs where AGEP < 50 and pwgtp1",
          "select * from acs",
          "select pwgtp1 from acs where AGEP < 50",
          "select pwgtp1 from acs")
exec <- sapply(cmds2, sqldf)

# Compute right answer
testAns <- acs[acs$AGEP < 50,]["pwgtp1"]

# Compare to find the correct answer
library("dplyr")
check <- sapply(exec, function(x) {all_equal(x, testAns)})
answ2 <- match(T, check)

# ------------------------------------------------------------------ Question 3
# Using the same data frame you created in the previous problem, what is the
# equivalent function to unique(acs$AGEP)

library("sqldf")

# Executed question commands
cmds3 <- c("select distinct pwgtp1 from acs",
          "select AGEP where unique from acs",
          "select distinct AGEP from acs",
          "select unique * from acs")
exec <- sapply(cmds3, function(x) {
                                    tryCatch({
                                       sqldf(x)
                                    }, error = function(e) {
                                       NA
                                    })
                                 })

# Compute right answer
testAns <- unique(acs$AGEP)

# Compare to find the correct answer
check <- sapply(exec, function(x) {identical(x, testAns)})
answ3 <- match(T, check)

# ------------------------------------------------------------------ Question 4
# How many characters are in the 10th, 20th, 30th and 100th lines of HTML
# from this page: http://biostat.jhsph.edu/~jleek/contact.html

# Retrieve the HTML content of a website
website <- "https://biostat.jhsph.edu/~jleek/contact.html"
urlConn <- url(website)
htmlCode <- readLines(urlConn)
close(urlConn)

# Compute answer
answ4 <- sapply(htmlCode[c(10, 20, 30, 100)], nchar)

# ------------------------------------------------------------------ Question 5
# Read this data set into R and report the sum of the numbers in the fourth of
# the nine columns.

# Download the data if it does not already exists
filePath <- paste("data", "wksst8110.for", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fwksst8110.for"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

# Load the file in a table
fixedTbl <- read.fwf(file   = filePath,
                     skip   = 4,
                     widths = c(10, 9, 4, 9, 4, 9, 4, 9, 4))

# Compute answer
answ5 <- sum(fixedTbl[[4]])

# -------------------------------------------------------------------- Summary

sprintf("Answer 1: '%s'", answ1)
sprintf("Answer 2: '%s'", cmds2[[answ2]])
sprintf("Answer 3: '%s'", cmds3[[answ3]])
sprintf("Answer 4: '%s'", paste(answ4, collapse = " "))
sprintf("Answer 5: '%f'", answ5)

