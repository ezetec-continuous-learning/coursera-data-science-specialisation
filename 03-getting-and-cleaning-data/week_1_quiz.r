# Week 1 Quiz

# Clear the environment
rm(list=ls())

# R and RStudio are stupid as there is no straightforward way to specify a
# the root folder to this script file
if( dir.exists("03-getting-and-cleaning-data") )
{
   # Assume the working directory is RStudio project root
   setwd(paste(getwd(), "03-getting-and-cleaning-data", sep = "/"))
}

# Create the data folder if it does not already exists
if( !dir.exists("data") )
{
   dir.create("data")
}

# ------------------------------------------------------------------ Question 1
# How many properties are worth $1,000,000 or more?

# Download the data if it does not already exists
filePath <- paste("data", "microdata-survey-2006.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2Fss06hid.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

# Load the data using CSV as the separator is "," by default
csv_tbl <- read.csv(filePath)

library("data.table")

# Convert to data.table as this was stressed by last week video
csv_tbl <- data.table(csv_tbl)

# Filter the number of rows by which properties are worth $1,000,000 or more
answ1 <- nrow(csv_tbl[csv_tbl$VAL == 24,])
sprintf("Answer 1: %d" , answ1)

# ------------------------------------------------------------------ Question 2
# Consider the variable FES in the code book. Which of the "tidy data"
# principles does this variable violate?

sprintf("Answer 2: %s" , "Tidy data has one variable per column")

# ------------------------------------------------------------------ Question 3
# What is the value of:
#    sum(dat$Zip*dat$Ext,na.rm=T)

# Download the data if it does not already exists
filePath <- paste("data", "nat-gas-program.xlsx", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2FDATA.gov_NGAP.xlsx"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

library("xlsx")

# Read rows 18-23 and columns 7-15 into R and assign the result to a variable
# called: dat
dat <- read.xlsx(file       = filePath,
                      sheetIndex = 1,
                      colIndex   = 7:15,
                      rowIndex   = 18:23)

# Compute answer
answ3 <- sum(dat$Zip*dat$Ext,na.rm=T)
sprintf("Answer 3: %d" , answ3)

# ------------------------------------------------------------------ Question 4
# How many restaurants have zipcode 21231?

library("XML")

# Download the data if it does not already exists
filePath <- paste("data", "baltimore-restaurants.xlm", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2Frestaurants.xml"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

# Read the XML data on Baltimore restaurants
xml_tree <- xmlTreeParse(file             = filePath,
                         useInternalNodes = TRUE)

# Gather the XML root node
xml_root <- xmlRoot(xml_tree)

# Gather the zipcode values
zipcodes <- xpathSApply(xml_root, "//zipcode", xmlValue)

# Compute answer
answ4 <- sum(zipcodes == "21231")
sprintf("Answer 4: %d" , answ4)


# ------------------------------------------------------------------ Question 5
# Using the data.table package, which will deliver the fastest user time?


# Download the data if it does not already exists
filePath <- paste("data", "microdata-survey-2006-2.csv", sep = "/")
if ( !file.exists(filePath) )
{
   fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fdata%2Fss06pid.csv"
   download.file(url      = fileUrl,
                 destfile = filePath,
                 method   = "curl")
}

# Load the data using CSV as the separator is "," by default
# Use the fread() command load the data into an R object DT
DT <- fread(filePath)

# Create commands table
commands = c("DT[, mean(pwgtp15), by=SEX]",
             "mean(DT$pwgtp15, by=DT$SEX)",
             "tapply(DT$pwgtp15, DT$SEX, mean)",
             "sapply(split(DT$pwgtp15, DT$SEX), mean)",
             "{mean(DT[DT$SEX==1,]$pwgtp15); mean(DT[DT$SEX==2,]$pwgtp15)}",
             "{rowMeans(DT,na.rm = T)[DT$SEX==1]; rowMeans(DT,na.rm = T)[DT$SEX==2]}")

for ( cmd in commands )
{
   print(cmd)
   print(eval(parse(text=cmd)))
   print(eval(parse(text=paste("system.time(", cmd, ")", sep=""))))
}
